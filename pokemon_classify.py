# Title:    Classification of legendary pokemon
# Autor:    Domagoj Kovac
# Dataset:  Pokemon with stats [https://www.kaggle.com/abcsds/pokemon]

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from sklearn import svm
from sklearn import metrics
from sklearn import linear_model
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler

# Full path to dataset
PATH = "/home/domagoj/Files/FAKS/Diplomski/DRB1617/RUSU/pokemon/Pokemon.csv"
fhand = pd.read_csv(PATH)

# Function to plot confusion matrix with values for TP, TN, FP, FN
def plotConfusionMatrix(c_matrix):
  norm_conf = []
  for i in c_matrix:
    a = 0
    tmp_arr = []
    a = sum(i, 0)
    for j in i:
      tmp_arr.append(float(j)/float(a))
    norm_conf.append(tmp_arr)
  
  fig = plt.figure()
  ax = fig.add_subplot(111)
  res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
  
  width = len(c_matrix)
  height = len(c_matrix[0])
  
  # Input text with annotate() function
  for x in xrange(width):
    for y in xrange(height):
      ax.annotate(str(c_matrix[x][y]), xy=(y, x),
      horizontalalignment='center',
      verticalalignment='center', color = 'b', size = 20)

  fig.colorbar(res)
  numbers = '0123456789'
  plt.xticks(range(width), numbers[:width])
  plt.yticks(range(height), numbers[:height])

def plotSVM(X, y, h, models, titles=None):
    # Create meshgrid
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    plt.figure()
    # Iterate over classifiers
    m = len(models)-1
    for i, model in enumerate(models):
	if (len(models)-1 == 0):
	    m = 1
	plt.subplot(m, len(models), i + 1)
	Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
     
	# Put the result into a color plot
	Z = Z.reshape(xx.shape)
	plt.contourf(xx, yy, Z, cmap=plt.cm.Greys, alpha=0.4)
     
	# Plot also the training points
	plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.binary)
	plt.xlim(xx.min(), xx.max())
	plt.ylim(yy.min(), yy.max())
	plt.xticks(())
	plt.yticks(())
	if (titles != None):
	    plt.title(titles[i])

# Pokedex number and name don't matter
features = ['Total', 'HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed', 'Generation', 'Legendary']
# Normalize values
X_norm = fhand.copy()
X_norm[features] = StandardScaler().fit(fhand[features]).transform(fhand[features])
X_tSNE = TSNE(learning_rate = 500, n_components = 2, perplexity = 30).fit_transform(X_norm[features])

X = X_tSNE # Input data
y = fhand['Legendary'] # Target column

# X2, y2 values are for logistic regression
dataset = fhand.sort_values(by='Total')
X2 = dataset['Total'].values.reshape((-1,1))
y2 = dataset['Legendary'].values.reshape((-1,1))

# Separate datasets into training and testing
np.random.seed(12)
index = np.random.permutation(len(X))
index_train = index[0:int(np.floor(0.8*len(X)))]
index_test = index[int(np.floor(0.8*len(X)))+1:len(X)]

# Dataset for SVM made as output from t-SNE
X_train, y_train = X[index_train], y[index_train] 
X_test, y_test = X[index_test], y[index_test]

# Dataset for logistic regression
X2_train, y2_train = X2[index_train], y2[index_train]
X2_test, y2_test = X2[index_test], y2[index_test]

# Weights for all models need to be set at 25% of original value because of outlier non-legendary pokemon
model = linear_model.LogisticRegression(C=1e5, class_weight={0: 0.25, 1: 1}).fit(X2_train,y2_train)
y_log = model.predict(X2_test).flatten()
y_true = y2_test.flatten()
correct = (y_true == y_log) # Get correctly predicted values

# Plot boundry for logistic regression (red color for uncorrect prediction) 
plt.figure()
plt.scatter(X2_test,y_log,color=['black' if point else 'red' for point in correct])
Xt = np.linspace(150,900,300)
boundry = (1/(1 + np.exp(-(model.intercept_ + Xt*model.coef_)))).ravel()
plt.plot(Xt, boundry, color = 'k')
plt.grid()

# Support Vector Machine models
rbf_model = svm.SVC(kernel='rbf', gamma=0.7, C=1, class_weight={0: 0.25, 1: 1}).fit(X_train, y_train)
lin_model = svm.LinearSVC(C=1, class_weight={0: 0.25, 1: 1}).fit(X_train, y_train)

y_rbf = rbf_model.predict(X_test).flatten()
y_lin = lin_model.predict(X_test).flatten()
y_true = y_test

correct1 = (y_true == y_rbf)
correct2 = (y_true == y_lin)

# Get confusion matrices
confMat_rbf = metrics.confusion_matrix(y_true, y_rbf)
confMat_lin = metrics.confusion_matrix(y_true, y_lin)

# Plot matrices
plotConfusionMatrix(confMat_rbf)
plotConfusionMatrix(confMat_lin)

# Plot SVM decision boundries
plotSVM(X_train, y_train, 0.02, [rbf_model, lin_model], ['RBF','Linear'])
plotSVM(X_test, y_test, 0.02, [rbf_model, lin_model], ['RBF','Linear'])

plt.show()

