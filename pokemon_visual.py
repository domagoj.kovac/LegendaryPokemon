# Title:    Visualization of legendary pokemon
# Autor:    Domagoj Kovac
# Dataset:  Pokemon with stats [https://www.kaggle.com/abcsds/pokemon]

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

# Full path to dataset
PATH = "/home/domagoj/Files/FAKS/Diplomski/DRB1617/RUSU/pokemon/Pokemon.csv"
fhand = pd.read_csv(PATH)

# Plot pie chart of non-legendary and legendary pokemon
legendary = fhand[fhand.Legendary]
totalNonLegendary = len(fhand)-len(legendary)
totalLegendary = len(legendary)
plt.pie([totalNonLegendary, totalLegendary], labels=['Non-legendary', 'Legendary'], colors=['white', 'black'], autopct='%1.1f%%')

# Plot attack vs. defense stats of all pokemon
legendary_sorted = legendary.sort_values(['Total'], ascending = [1])
non_legendary = fhand[fhand.Legendary == False]
non_legendary_sorted = non_legendary.sort_values(['Total'], ascending = [1])

fig = plt.figure()
fig.add_subplot(121)
plt.scatter(legendary.Attack, legendary.Defense, c = 'k', marker = 'o')
plt.scatter(non_legendary.Attack, non_legendary.Defense, c = 'w', marker = 'o')
plt.xlabel('Attack')
plt.ylabel('Defense')
plt.grid()
fig.add_subplot(122)
plt.scatter(legendary['Sp. Atk'], legendary['Sp. Def'], c = 'k', marker = 'o')
plt.scatter(non_legendary['Sp. Atk'], non_legendary['Sp. Def'], c = 'w', marker = 'o')
plt.xlabel('Special Attack')
plt.ylabel('Special Defense')
plt.grid()

# Reduce with different t-SNE parameters
features = ['Total', 'HP', 'Attack', 'Defense', 'Sp. Atk', 'Sp. Def', 'Speed', 'Legendary']
X_norm = fhand.copy()
X_norm[features] = StandardScaler().fit(fhand[features]).transform(fhand[features])
X_PCA = PCA().fit_transform(X_norm[features])
X_tSNE1 = TSNE(learning_rate = 500, n_components = 2, perplexity = 5).fit_transform(X_norm[features])
X_tSNE2 = TSNE(learning_rate = 500, n_components = 2, perplexity = 10).fit_transform(X_norm[features])
X_tSNE3 = TSNE(learning_rate = 500, n_components = 2, perplexity = 30).fit_transform(X_norm[features])
X_tSNE4 = TSNE(learning_rate = 500, n_components = 2, perplexity = 100).fit_transform(X_norm[features])

cmap = plt.get_cmap('binary')

# Plot PCA
figt1 = plt.figure(figsize=(12,5))
figt1.add_subplot(121)
plt.scatter(X_tSNE3[:,0], X_tSNE3[:,1], c = cmap(fhand['Legendary']*1.))
plt.grid()
figt1.add_subplot(122)
plt.scatter(X_PCA[:,0], X_PCA[:,1], c = cmap(fhand['Legendary']*1.))
plt.grid()

# Plot t-SNE results as one figure
figt = plt.figure()
figt.add_subplot(221)
plt.scatter(X_tSNE1[:,0], X_tSNE1[:,1], c = cmap(fhand['Legendary']*1.))
plt.grid()
figt.add_subplot(222)
plt.scatter(X_tSNE2[:,0], X_tSNE2[:,1], c = cmap(fhand['Legendary']*1.))
plt.grid()
figt.add_subplot(223)
plt.scatter(X_tSNE3[:,0], X_tSNE3[:,1], c = cmap(fhand['Legendary']*1.))
plt.grid()
figt.add_subplot(224)
plt.scatter(X_tSNE4[:,0], X_tSNE4[:,1], c = cmap(fhand['Legendary']*1.))
plt.grid()

plt.show()
